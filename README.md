versio 1.0

# EPG README #

Elektroninen prosessiopas, joka koostuu Dojo Toolkitilla toteutetusta asiakasohjelmasta, Node.js-alustalla olevasta palvelinpään ratkaisusta, sekä MongoDB-dokumenttitietokannasta.

###Tietokanta:###

* vsetest.json - MongoDB-tietokannan kokoelman vsetest sisältö

###Palvelinpää:###

* nodevse - Node.js-projekti, johon on ladattu Express- ja Node MongoDB Driver -moduulit.

###Asiakasohjelma:###

* epgv1 - Netbeans-projekti, johon tarvitsee vielä purkaa Dojo-kirjasto, jotta ajaminen onnistuu
* testi4 - sama kuin epgv1, mutta mukana on vielä kehityksen aikaisia kommentteja
* dojo-release-1.9.1.zip - Dojo Toolkit -kirjasto

##Käyttöönotto##

###Tietokanta:###

Asenna MongoDB (http://www.mongodb.org/). Sijoita vsetest.json-tiedosto MongoDB:n asennushakemiston bin-hakemistoon ja mongo-shelliin kirjoitetaan komento:
mongoimport --db test --collection vsetest --file vsetest.json

###Palvelinpää:###

Asenna Node.js (http://nodejs.org/). Sijoita nodevse-hakemisto esimerkiksi C:\-juureen, jonka jälkeen siirry komentokehotteella kyseiseen kansioon ja syötä komento:
node server

###Asiakassovellus:###

Asenna Netbeans (https://netbeans.org/) ja tuo joko epgv1- tai testi4-hakemistot Netbeans-projektiksi. Pura dojo-release-1.9.1.zip-paketin sisältö (dojo, dijit, dojox) projektiin public_html-hakemistoon.
Aja Netbeans projekti.