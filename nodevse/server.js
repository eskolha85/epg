var express = require('express'),
	//wine = require('./routes/wines');
	//ladataan guides.js
	epg = require('./routes/guides');
 
var app = express();

//viritys CORSin kiertämiselle
//http://stackoverflow.com/questions/11001817/allow-cors-rest-request-to-a-express-node-js-application-on-heroku
//lisätään headeriin tavaraa, jolla sallitaan cross origin resource sharing
//ongelma ilmeisesti varsinkin localhostilla
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
 
app.configure(function () {
	app.use(allowCrossDomain);
	app.use(express.logger('dev')); /* 'default', 'short', 'tiny', 'dev' */
	app.use(express.bodyParser());
});
 
//app.get('/wines', wine.findAll);
//app.get('/wines/:id', wine.findById);
//app.post('/wines', wine.addWine);
//app.put('/wines/:id', wine.updateWine);
//app.delete('/wines/:id', wine.deleteWine);

app.get('/guides', epg.findAll);
app.get('/guides/:id', epg.findById);
 
app.listen(3000);
console.log('Listening on port 3000...');