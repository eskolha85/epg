var mongo = require('mongodb');
 
var Server = mongo.Server,
Db = mongo.Db,
BSON = mongo.BSONPure;
 
var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('test', server);
 
db.open(function(err, db) {
	if(!err) {
		console.log("Connected to 'test' database");
		db.collection('vsetest', {strict:true}, function(err, collection) {
			if (err) {
				console.log("The 'vsetest' collection doesn't exist...");
				//populateDB(); tätä voisi käyttää hyväksi - ei tarvitse mongoimport
			}
		});
	}
});
 
exports.findById = function(req, res) {
	var id = req.params.id;
	console.log('Retrieving guide: ' + id);
		db.collection('vsetest', function(err, collection) {
			//collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item) { //tämä jos mongodb on generoinut _id -kentän
			collection.findOne({'_id': id}, function(err, item) {
				res.send(item);
			});
		});
};
 
exports.findAll = function(req, res) {
	db.collection('vsetest', function(err, collection) {
		collection.find().toArray(function(err, items) {
			res.send(items);
		});
	});
};
 
//exports.addGuide = function(req, res) {
//	var guide = req.body;
//	console.log('Adding guide: ' + JSON.stringify(wine));
//	db.collection('vsetest', function(err, collection) {
//		collection.insert(guide, {safe:true}, function(err, result) {
//			if (err) {
//				res.send({'error':'An error has occurred'});
//			} else {
//				console.log('Success: ' + JSON.stringify(result[0]));
//				res.send(result[0]);
//			}
//		});
//	});
//}
 
//exports.updateGuide = function(req, res) {
//	var id = req.params.id;
//	var guide = req.body;
//	console.log('Updating guide: ' + id);
//	console.log(JSON.stringify(guide));
//	db.collection('vsetest', function(err, collection) {
//		collection.update({'_id':new BSON.ObjectID(id)}, guide, {safe:true}, function(err, result) {
//			if (err) {
//				console.log('Error updating guide: ' + err);
//				res.send({'error':'An error has occurred'});
//			} else {
//				console.log('' + result + ' document(s) updated');
//				res.send(guide);
//			}
//		});
//	});
//}
 
//exports.deleteGuide = function(req, res) {
//	var id = req.params.id;
//	console.log('Deleting guide: ' + id);
//	db.collection('vsetest', function(err, collection) {
//		collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
//			if (err) {
//				res.send({'error':'An error has occurred - ' + err});
//			} else {
//				console.log('' + result + ' document(s) deleted');
//				res.send(req.body);
//			}
//		});
//	});
//}
 
/*--------------------------------------------------------------------------------------------------------------------*/
// Populate database with sample data -- Only used once: the first time the application is started.
// You'd typically not find this code in a real-life app, since the database would already exist.
//var populateDB = function() {
// 
//	var wines = [
//	{
//		name: "CHATEAU DE SAINT COSME",
//		year: "2009",
//		grapes: "Grenache / Syrah",
//		country: "France",
//		region: "Southern Rhone",
//		description: "The aromas of fruit and spice...",
//		picture: "saint_cosme.jpg"
//	},
//	{
//		name: "LAN RIOJA CRIANZA",
//		year: "2006",
//		grapes: "Tempranillo",
//		country: "Spain",
//		region: "Rioja",
//		description: "A resurgence of interest in boutique vineyards...",
//		picture: "lan_rioja.jpg"
//	}];
// 
//	db.collection('wines', function(err, collection) {
//		collection.insert(wines, {safe:true}, function(err, result) {});
//	});
// 
//};