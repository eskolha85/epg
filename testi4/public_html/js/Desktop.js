define([
    "dojo",
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_AttachMixin", "dijit/layout/_LayoutWidget", "dijit/_Container","dijit/_Templated",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/store/JsonRest",
    "dojo/store/Memory",
    "dojo/store/Cache",
    "dijit/Tree",
    "dijit/registry",
    "dijit/layout/BorderContainer",
    "dijit/layout/ContentPane",
    "dojo/text!./templates/desktop.html"
], function(
dojo,
declare,
_WidgetBase,
_AttachMixin,
_LayoutWidget,
_Container,
_Templated,
_TemplatedMixin,
_WidgetsInTemplateMixin,
JsonRest,
Memory,
Cache,
Tree,
registry,
BorderContainer,
ContentPane,
template){

    return declare("Desktop", [BorderContainer, ContentPane, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: template,
        widgetsInTemplate: true,
        
        constructor: function() {
              console.log(this.containerNode);  //null
              //console.log(this.bcPane); //undefined
        },
        
        postCreate: function(){
            // do something after desktop template ready. Note: My children might not be 
            // ready yet, so dont' access them here!
            this.inherited(arguments);
            console.log("postCreate");
            //console.log(this.bcPane);
            //console.log(this.containerNode);
            

        },
        startup: function() {
            // This fires after my children are available
            this.inherited(arguments);
            console.log("startup");
            
            var reststore = new JsonRest({
                //target:"data/",         // << adapt URL
                target: "http://localhost:3000/guides/",
                mayHaveChildren: function(object){
                    // if true, we might be missing the data, false and nothing should be done
                    return "children" in object;
                },
                getChildren: function(object, onComplete, onError){

                    // this.get calls 'mayHaveChildren' and if this returns true, it will load whats needed, overwriting the 'true' into '{ item }'
                    this.get(object._id).then(function(fullObject){
                        // copy to the original object so it has the children array as well.
                        object.children = fullObject.children;
                        // now that full object, we should have an array of children
                        onComplete(fullObject.children);
                    }, function(error){
                        // an error occurred, log it, and indicate no children
                        console.error(error);
                        onComplete([]);
                    });
                },
                getRoot: function(onItem, onError){
                    // get the root object, we will do a get() and callback the result
                    //this.get("VSETEST").then(onItem, onError);
                    this.get("vse-tree").then(onItem, onError);
                },
                getLabel: function(object){
                    // just get the name (note some models makes use of 'labelAttr' as opposed to simply returning the key 'name')
                    return object.name;
                }
            });
            
            //memoryStore = new Memory();
            //store = new Cache(reststore, memoryStore);
            
            //var id = "roles-tree";
            //console.log(reststore.get(id));
            
            var myTree = new Tree({

                model: reststore, //reststore suoraan tänne, ei modelin kautta
                showRoot: true,
                
                //onLoad() Called when tree finishes loading and expanding.
                onLoad: function() {
                    
                  console.log("Tree onLoad()");
                  //haetaan widgettiä dijit-rekisteristä sen nimellä
                  registry.byId("borderContainer").resize();
                  //this.bcPane.resize(); //taustalta vaikuttaisi katoavan koko bordercontainer
                  //this.resize(); -- ei toimi
                 
                },
                
                //onOpen(item, node) Callback when a node is opened
//                onOpen: function() {
//                    console.debug("Avataan puun node");
//                    //haetaan widgettiä dijit-rekisteristä sen nimellä
//                    registry.byId("borderContainer").resize();
//                },
//                
//                //onClose(item, node) Callback when a node is closed
//                onClose: function() {
//                    console.debug("Suljetaan puun node");
//                    //center contentpane ei aivan ehdi mukaan - resize tapahtuu leading panelle - center panelle liian aikaisin? - lisätään viive?
//                    //pitäisikö myös säätää center ja leading panelle max & min width?
//                    //haetaan widgettiä dijit-rekisteristä sen nimellä
//                    
//                    //var delay=300;//0,3 seconds
//                    //setTimeout(function(){
//                        registry.byId("borderContainer").resize();
//                    //},delay);
//                    //registry.byId("borderContainer").resize();
//                },

                //onClick(item, node, evt) Callback when a tree node is clicked
                onClick: function(item) {
                    
                    for (var i = 0; i < item.items.length; i++) {
                        var group = item.items[i];

                        var allPropertyNames = Object.keys(group);
                        for (var j=0; j<allPropertyNames.length; j++) {
                            var name = allPropertyNames[j];
                            var value = group[name];
                            // Do something
                            console.log(name);
                            //console.log(value);
                        }
                    }

                    //kelataan läpi itemin childrenien nimet - testi
//                    for(var i=0;i<item.children.length;i++){
//                        console.debug(item.children[i].name);
//                    }
                    //linkitys jsonrest - get(id, options) - tämä linkiksi?

                    //console.debug(Object.prototype.toString.call(item.description)); //object Undefined

                    var str = item.description; //täytyi ottaa talteen muuttujaan ensin - muuten ei toimi jos suoraan item.description.replace()
                    //var res = str.replace(/(\n)+/, '<br/>'); // muuttaa vain 1. \n --> <br/> - muut filtteröidään pois

                    //katsotaan onko "undefined" toimii myös jos muuttujaa ei esitelty
                    if(!(typeof str==="undefined")){
                        var res = str.replace(new RegExp('\r?\n','g'), '<br />');
                    }

                    //var res = str.replace(new RegExp('\r?\n','g'), '<br />'); //TOIMII!!!!! --> undefined seuraava ongelma
                    //RegExpillä voisi tehdä myös linkityksen?(termeistä linkkejä?)

                    //console.debug(item.description[0]); //ItemFileReadStorella
                    //console.debug("item._id: " + item._id);
                    //console.debug("item.description: " + item.description); //restillä
                    //dojo.byId('content').innerHTML = "<h1>" + item.name[0] + "</h1><p>" + item.description[0] + "</p>"; //ItemFileReadStoren kanssa tämä
                    //dojo.byId('content').innerHTML = "<h1>" + item.name + "</h1><p>" + item.description + "</p>"; // Tämä restin kanssa
                    //dojo.byId('content').innerHTML = "<h1>" + item.name + "</h1><p>" + item.description + "</p>" + "<img src='" + item.imagesource + "'/>"; // Tämä restin kanssa
                    //dojo.byId('contentId').innerHTML = "<h1>" + item.name + "</h1><p>" + res + "</p>" + "<img src='" + item.imagesource + "'/><hr>";
                    
                    //dojo.byId('contentId').innerHTML = "<h1>" + item.name + "</h1><p>" + res + "</p>" + "<img src='" + item.imagesource + "'/><hr>";
                    
                    //console.log("itemit: ");
                    
                    var content = "<h1>" + item.name + "</h1><p>" + res + "</p>";
                    
                    for(var i=0; i< item.items.length; i++){
                        //console.log(item.items[i].name);
                        
                        //löytyykö 'name' -key
                        if (!(typeof (item.items[i].name)==="undefined")){
                            content += "<h3>" + item.items[i].name + "</h3>";
                        }
                        
                        //löytyykö 'status' -key
                        if (!(typeof (item.items[i].status)==="undefined")){
                            
                            var s = item.items[i].status;
                            var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                            
                            content += "<p>" + r + "</p>";
                        }
                        
                        //löytyykö 'description' -key
                        if (!(typeof (item.items[i].description)==="undefined")){
                            
                            var s = item.items[i].description;
                            var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                            
                            content += "<p>" + r + "</p>";
                        }
                        
                        //löytyykö 'image' -key
                        if (!(typeof (item.items[i].image)==="undefined")){
                            content += "<img src='" + item.items[i].image + "'/>";
                        }
                        
                        //löytyykö 'table' -key
                        if (!(typeof (item.items[i].table)==="undefined")){
                            
                            content += "<table style='width:100%'>";
                            
                            for(var j=0; j< item.items[i].table.length; j++){
                                
                                if (j % 2 === 0 ){
                                    content += "<tr class='alt-row'>";
                                } else {
                                    content += "<tr>";
                                }
                                
                                
                                //console.log(item.items[i].table[j].name);
                                //löytyykö tablesta 'name' -key
                                if (!(typeof (item.items[i].table[j].name)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].name + "</th>";
                                    } else {
                                        content += "<td>" + item.items[i].table[j].name + "</td>";
                                    }
                                    
                                }
                                
                                //löytyykö tablesta 'description' -key
                                if (!(typeof (item.items[i].table[j].description)==="undefined")){
                                    //console.log("tablesta löytyi description");
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].description + "</th>";
                                    } else {
                                        var s = item.items[i].table[j].description;
                                        var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                                        //r = s.replace(new RegExp('\t','g'), '&nbsp;&nbsp;&nbsp;&nbsp;'); //tämän huomaa products - requirements spec taulukossa - kun korvataan tabeja rivivaihdot katoavat...
                                        content += "<td>" + r + "</td>";
                                    }
                                    
                                }
                                
                                //löytyykö tablesta 'source' -key
                                if (!(typeof (item.items[i].table[j].source)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].source + "</th>";
                                    } else {
                                        content += "<td>" + item.items[i].table[j].source + "</td>";
                                    }
                                    
                                }
                                
                                //löytyykö tablesta 'destination' -key
                                if (!(typeof (item.items[i].table[j].destination)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].destination + "</th>";
                                    } else {
                                        content += "<td>" + item.items[i].table[j].destination + "</td>";
                                    }
                                    
                                }
                                
                                //löytyykö tablesta 'role' -key
                                if (!(typeof (item.items[i].table[j].role)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].role + "</th>";
                                    } else {
                                        var s = item.items[i].table[j].role;
                                        var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                                        content += "<td>" + r + "</td>";
                                    }
                                    
                                }
                                
                                //löytyykö tablesta 'input_products' -key
                                if (!(typeof (item.items[i].table[j].input_products)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].input_products + "</th>";
                                    } else {
                                        var s = item.items[i].table[j].input_products;
                                        var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                                        content += "<td>" + r + "</td>";
                                    }
                                    
                                }
                                
                                //löytyykö tablesta 'output_products' -key
                                if (!(typeof (item.items[i].table[j].output_products)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].output_products + "</th>";
                                    } else {
                                        var s = item.items[i].table[j].output_products;
                                        var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                                        content += "<td>" + r + "</td>";
                                    }
                                    
                                }
                                
                                //löytyykö tablesta 'abbreviation' -key
                                if (!(typeof (item.items[i].table[j].abbreviation)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].abbreviation + "</th>";
                                    } else {
                                        content += "<td>" + item.items[i].table[j].abbreviation + "</td>";
                                    }
                                    
                                }
                                
                                content += "</tr>";
                            }
                            
                            content += "</table>";
                            
                        }
                        
                    }
                    
                    dojo.byId('contentId').innerHTML = content;
                    
                    //console.debug("item.description: " + res);
                    //dojo.byId('content').innerHTML = "<h1>" + item.name + "</h1><p>" + item.description + "</p>" + "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAe0AAAEWCAIAAADEg1h2AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAC5NSURBVHhe7Z0HVBXX/rYTKzEq9i5oVLBjBQvFClasEQuKHU0sJNFgBRVFjYolCooKduwdsBARG6JRUKJgBYyKJQm5SW5cd/3Xt/7fa/bc+fjGdpwZhOG8z3qX6zf77Jlz2OfMM3vmFD/6X0IIIUaGHieEEGNDj5McJNZcSUlJkYaAkJyHHic6k5aW5u/vb2dn99FHHzk6OZtnbGxsLSwsPDwGhoeHS+NCSI5BjxPdyMzMnDzZx8raeorvzJi4hCe/vTDnZGRmrd2wZeDgodbWNSIiIqQxIiQHoMeJPsTHxzdubBcQuEShM+ZyUmqffgM8PDxevHghDRYhukKPEx0ICwtzdHK5npquUBgjZ93GLfYODllZWdKQEaIf9DjRSlRUdMfOrhmZWQpzMYrExCVA5ZyVE92hx4kmEhMTMRO/lfZE4Szmtdm0bbd7r17S2BGiE/Q40YSdnR3f0nyvdOvhfuDAAWn4CNEDepyoBz6ClRSeYt6ey0mpNra2vLpCdIQeJ+qxd3CIPBGn8BTzzvTpN4CfRCQ6Qo8TlaSlpVlZWysMxZiStRu2eHgMlMaREM3Q40Qly5cv9x4/UWEoxpRkZGZZWFhI40iIZuhxopJp06bN9AtQGIoxMRUrVsrMzJSGkhBt0ONEJV5ew1euDlXoiTExjRrbJSYmSkNJiDbocaISZxeX/YePK/TEmBhHJ+fY2FhpKAnRBj1OVEKPawk9TnSEHicqoce1hB4nOkKPE5XQ41pCjxMdoceJSuhxLaHHiY7Q40Ql9LiW0ONER+hxohJ6XEvocaIj9DhRCT2uJfQ40RF6nKiEHtcSepzoCD1OVEKPawk9TnSEHicqoce1hB4nOkKPE5XQ41pCjxMdUenxj4g2pHE0MjntcWmk/kut2nV27j2i6PP2ODq5WFhYKBpNSeTx07PnLFA06ht6nOiIeo8rXpeM6cHoSeNoZD6Ax8tXqDh91hykd78BBQoU+OSTT27efajo9pYsXLJilv98ReM7c/ZiEu7a+4tJinZ9Q48THaHHcyH0uCnBKHXs7CYvjp/gg5bQsG1JN+5NmPwNipD1m/3nLRS3RsecRSOyYdOOR8//Eo1+cwN9vpkm6rRHvwUELkGHb6fPxhZEI3L6/BWx4vchGx4++zP9cdaoMeNxR02btUDj41/+LffUN/Q40RF6PBdCj5uS7B6HYfv2H4iWPQejT56OR1GxUmX8W6VqNdwKNaOuUfOzJk2bf/zxx51du2Zk/o52+boKJN6+Y+dChQo5t+tQomRJ6xo1f7x+C+07dh/CilWrVW/e0gHz/Z69+t5/+Gv16lZoLFKkSPESJXC/6JYToceJjtDjuRCMnjSORuYDeBzObdS4CSLcWq9+Q3hWeBxSDt+6a++hYwlXb2LRoVXbB0/+hbXmL1qGxanTZqGWPY6JORrXrAtDffZiEhw9cPBQ1FZW1jgeYJuoPQZ5dunWM/lWBq+rEMNBj+dCMHrSOBqZD+DxwoULly1bDrGtW2/EKO/k1HS0C487OrcT3Vav3YjF75Z9LxZT7z8u9umn4lbZ43A0+nzuMWTUmPEIGmvXsbmVllmwYEGX9h3FinLocWI46PFcCEZPGkcj84Gvj8sRHpcvfAuPL1gcJBbvpD8tXqIEpueoFR6f4jtz0dKVIitWhyo8fjfj2U+3H6Cgx4nhoMdzIRg9aRyNTB7x+PlL1zFtx4T95t2Hj57/NXXaLNw6f9Ey3CR7fOGSFWgUn12Jjjnbq0//OQGLUGOtEiVLJt24l/nr3127uxcqVOj0+SvC4yNHj0OHnAs9TnSEHs+FYPSkcTQyecTjyAy/eVB5uXLlq1V7eRndyaX9z0//QLvscUy9m7d0gKZr1Pzs0+LFK1aqHHfhKtq37zpYtGjRMmXKWlnXwIoDBno+fPbn7fQn2BTa0ZnvcxJDoKfHH//y76HDR2GPAja2dSP2HBbtOPNNufdI7pY9OAteuSYH/8/1yBNxiKLx1Uyb6Y+dfMQob7EYHBrerUcv+dZmzVtWrlJVvI2GYBIn3/TatGnrtP/I2wSH0ZPG0cjktMfxwti4OULRiOC1hJv2/f93HRN3EdNtPIO79h2VPywoexy59/Mv34dsGDh4qN/cQEzhRSNyJj7xm29nenqNhNNla2NraJk7fzHm6XJPfUOPEx3R0+OYIrm6dcMOg/pE7IXSpctcvHIDtUOrtm+a1+w9dGzSV98qGnXM6LFfRJ08o2h8NbXr2Jw6e0levJvxrEiRIqK+cednzMtaOrQWMzikcpUqonhT6PFcz0+3H8wJWASJY6KtuCmPhB4nOqKnxyHNcV9OlmdDoWHbMPFZsDjI0rJUk6bNfziT8OP1W127uzdt1gK6v3A5Gbps1LgJTnK9Ro5B/yVBq3ETpB/43XLMg3A86N338zXrwtCIDli9S7eeqMO27ERn3AsOG1h0ad9x7YYtaMExA9OxST5T0Yh7uZp8Z/f+yEqVq2D6vG7jVvGQxIqYkTVrYY9Zdsj6zWjBKp988kn9Bo2yf+0bDzj23GUUIaGbxoybELBw6fTZc7GIM/QKFSsdPRbbw70P+uDf5FsZaMc28ZeiZdmKYNnjaBnt/SUKRQzn8dikp//5n/8jLfyXvOxxzMoxyMWKFZPf/8xroceJjujpccxYGzayq1uv/hcTv9q284D4LsalpNR69Rti3n3vwfPWbZwgdxgZ6nRyaZ/+OGvx0lU9e/WFglcFr+/Y2Q03Yfrc2bXrngNRWLdEyZJfT52Bk1yoGeseiT61Pnx7tWpWD578C1uAtdEfxmxh3wr3cu3m/YIFCy5bGYJG9979oP7k1HQUC5eswJxafpBC4idPx2NFWP7A0RM42FhZWeMBw9FyNxyQcDhB0atP/03bdsee+xHex9Fl6879OAjhyIQHjDtCN2wN3ab4zsTDQAvuFB7HIQQGx2MWZyeKGM7jrr6xPWbGBe64AaH/+ff/iMa87HGc/+H4mv0JzWuhx4mO6OlxBLNdmPHb6bOhsM9q1RbXVSBocV0FPkUORp70neFXu44NWo79cE5cV4HNkeEjxyKYTffuNwCNmCOL2f1Enylz5y9GgTi364B/cWwYMmyE6N/W0XnqtFnwuPwZskNRMVgFxeSvfRXXVdAn8vhpUQcELsFxAgUeTEJiimgU2RKxT3y7D4q/nf4ELQ0aNj57MQmHKE+vkXh8cs9y5cqjAzyOmbtogcfxt5csafkmj0CLhs6cLcnHf8x07uCWZz2e90OPEx3R0+MQq2xDiBu+g/VQC4/DyBA0tNjDvc/AwUMVHm/p0Nrnm2mYTYtgdoxGoWzkVY+XKVMWE225P6bq8PiAgZ6iz4nYC2/x+IXLyaL+PmTDmzyOswecWOD03LVLd9GCnrP85+M0Yv6ipeJCkEgdG1vhcfmaODyOPxyuF59+ezVGnI9nz/iVl3fGZrh06UePqw49TnRET4/DX2PGTRCf64C4oW8hMhTQIuxZpWo18YGwmX4BssfFFy4gXCF9BHoVV7Tf4vHuPXsHrVorWmbPWYA5/ps8jvMD0Sgyasx4NGb++nf646xOrl2CQ8PR+KrHkXYdOjk6t1u0dKVYjD13uXlLB8gds/LyFSqm3n+MxqPHYitUrIRC4XHUyanpOGhdSkoVjdljUI9PXZe4/+zLK1SiMS9fV8n7oceJjujpcTjLtm49pG//gVbWNTDzvZP+FO0wr12TZlA2JAjHQeudXbvC6TDp4agfLCwsINbEn+5ixbaOzuhcq3ad85euZ2T+jkm6+OCX7HEcHtD46Plfx0+dr17dCpvCYtNmLWDVhKs35csd2Kx4g/HLSV9b16gZvnWXaEeSb2XUq9+wQydX8S6lOK681uO406JFi2b/bby69RuI3+WY4TevarXqffp7WFqWEpfycUfbdx0U3YTHUWzcHOHQqm3ao99EuxzDefxI/KPf/viPtPBfTPc4/l4BnhfRgpMY0bJ7f6TczejBi3Cwp5e4hPjO0ONER/T0OILJeNyFq+vDt1++lioubSP3H/66YdMOzJdxK2QnPgdy8+5D4WgYWezMuPVI9ClMn+F3LGJ/EB8FQe79/IuwIbaJRrFlbBYr4vAg9hx4X/5xanQW82VsM2LP4TPxiaJdBCtCvtExZ4XEkZR7j+RHKwd3mv0NUuR2+hOcWIgapxehYdvkK+Ao5Lc0UctbxqOVazkYPWkcjYwKj0+b6Y/FK9dvFylSRLTkJ487OrngLxLno+8MPU50RGePM6YEoyeNo5F5L49jAl6qVGnx81VrN2xBS83PauFf4XEcd2f6BYgfsQpatVYcU0+dvYRFHIZDQjeh+Hb6bHFsRnC6M2bcBDR+PXVG7LkfRSPWWrp8DRpXr924a99RFCdiL6Ad04W9h45hEUEhZg/rNm7FIg7w4n7xMLBx3AVq+f1qBOeFOC1DIx6V+PzV3YxnONUL/G455gFoH/flZPGtohWrQ3Hmh7+odRtHefW3hB4nOkKP50IwetI4Gpn38ngn1y4dOrlaWpaCB4eNGI0W8f9CQKDwb7sOnYoVK9a9Z++69RugUfzvEJEn4lDXqPlZW0fnho3sUHsMevn+R9iWnQULFrRr0sytaw8LC4uyZcvhjAcbGTHKG32aNW/p0KptmTJlUYu3Rny+mYa6UeMm2A4K8YV+3xl+qKtUrebq1q14iRKFCxdGjUdYrnwFtO85GI0+O/cewfYrVKzk3rtfoUKFWrdxEnNtnExUqlwFD6B9x87o/Fmt2o+e/wWn40/AYvHiJdDnnaHHiY7Q47kQjJ40jkbmfT0+y38+ioORJ2vXsalVu87c+YuxKDy+ck2oeMP56LFYNPbtPxC18Lh4zyPl3iNM56tXt0J9KCoG02Fxqc25XQf0OXk6PiExBUUL+1bYGrQOlWMRHr928z60CwU/fPYnAvNi8f7DX4XHl60IxgZFLd5mxzwd9VdTpmPa3tiu6afFi4sfy8W0He07dh9CjS2ILzHgvtq0dUL75Wsv383u2t0dNa+rkA8PPZ4LwehJ42hk3tfj0TFnUcDRmE0PGTYiIHAJFsV1laiTZwYNGWZdo+Ynn3yCxuEjx6JReBySFRupW6++8HhG5u+YsGPuXLp0GXQA8Li4ViN/hEkcM+BxbB9F1WrVcTxAMLvHIhqFu8Vv74h1vw/ZgFr8CBc8joMBJulQtlixpUNrtH856Wv0QaP8SSo8VLTT4yR3ocdzIRg9aRyNzPt6/E76U3HlASxaulL2eEzcRfFLhEuCVm/YtAONb/c4jI92j0Gem7fvGTp8FGrId/uugyjEbycg4loKPI7JOwpXt25bIvbJSb6V8U6PY/KOyXiVqtWyr3j6/BX0ocdJXoMez4Vg9KRxNDLv63EU9q3aoAZn4hNlj2MSjUJ8jUD8UPjbPV6hYiXkp9sPbqc/EW8tQr4p9x5hLl+uXHls+fyl6+iAdng87dFv0HHNz2pdT0lLf5xl16RZuw6dULzT46jbd+yMKfnByJOoB3t6tXV0PhJ9CvWbPM7Pq5Dcgh7PhWD0pHE0Mu/l8Y7//JL45K99UVetVv3np38Ij+/ad/Tm3YfibUlQrZpVpcpVrKysMR2OPH4aLbLHbevWEx73mxv48ccf46YCBQqID72In7dd/v264iVKYLF06TI93PugEO9zhqzfbGlZCosAE3/xy1niv2YWHkcH1MLjJ2IvoBYex/FAXIcBuC/33v3wqDJ//Tu7x71GjsGtwuNDho0QncVNbw89TnSEHs+FYPSkcTQypns8Oubs8VPnUWDWjFr8RHByajrqxJ/uosbMGjLdtG13RubvCVdvov3R87+u3byPQnwPC4uofziTgBrBHNl/3sK4C1fvpD9Fe/yPP2HFLRH7tu86CDXfSsucPnsuBnnzjr2iP+53zbowTPaFcJFzCdewovjIPxpRiy8fZH9UCO53284DuC80ihasghqri8XYcz9iUczB7z/8FUeUyab9DjM9TnREvceJFqRxNDKme/wDBO4Wb3uOGjN+xepQzOgxa87+Xdy8Fnqc6Eh+EArJFfKUxxHMi13ad6xe3QpxaNVWXFTJs6HHiY7Q40Qlec3jxgo9TnSEHicqoce1hB4nOkKPE5XQ41pCjxMdoceJSuhxLaHHiY7Q40Ql9LiW0ONER+hxohJ6XEvocaIj9DhRCT2uJfQ40RF6nKiEHtcSepzoCD1OVEKPawk9TnSEHicqoce1hB4nOkKPE5XQ41pCjxMdoceJSuhxLaHHiY7Q40QlXl7DV64OVeiJMTEtWjrEx8dLQ0mINuhxopJp06bN9AtQ6IkxMVbW1mlpadJQEqINepyoZOHChZN8pij0xJiSjMwsCwsLaRwJ0Qw9TlSSkpJiY2OrMBRjSnbsOeTm5iaNIyGaoceJemxt655LSFJIinlnBg4eGh4eLg0iIZqhx4l6goNDho8co5AU8/ZcT023trZ+8eKFNIiEaIYeJ5qws7OLiZP++2PGlHiPnxgUtFwaPkL0gB4nmoiNje3Y2S0jM0thK+a12X/4uKubGyfjRF/ocaKV4OBgXl0xJZeTUhs3tsvKypIGjhCdoMeJDvj5+fftN4Cz8rck8kScvb0DPzNOcgJ6nOjDjh0RjRrb8Zv6r+ZW2pOpvrOcnV0yMzOlwSJEV+hxohspKSnOLi4dO7mGb93NuTlyOSl1pn+Aja2tv78/r4mTnIMeJzoTHR3dq1cvS8tSjk7OffoNmOI7M1fiMchzos83isYPk0k+U/C329jYWlvXmDZtGqfhJKehx0mOgOlnbGxsREQEpqIfnqFDh7Vu41i/fgNfX1+p6QOycOFC/O04O5HGgpAchh4n+Y2oqOjuPdwzMrNi4hKcnV34+RCS76HHSb5Clri4Qk2VE3OAHif5B4XEqXJiJtDjJJ8QFhbWsbPraz8nI1TO9xtJfoUeJ/kBSHzQkGEKfWcPVM6v4ZD8Cj1ODM87JS5yOSnVydmFKif5D3qcGBsTJS5ClZN8CT1ODExQUJDpEhehykn+gx4nRsXPz997/ESFpk2JUDm/p0PyDfQ4MSSQ+NRpsxSCNj1QeUt7h8TERGlzhBgZepwYD40SF7mV9sTRyYUqJ/kAepwYDF0kLkKVk/wBPU6MxDAvL70kLkKVk3wAPU4MAyS+as16hYi1hyonRoceJ8YghyQuApW3tHeIioqW7owQQ0GPEwOQoxIXycjM6t7DnSonRoQeJ3maFy9eeHh4NGxkp/g/dxQx5f8Fhaln+gUoVsyeiT5TLC1LUeXEcNDjJE+TlpYm/S87b8bHx8fK2lph7VezcnWoXZMm0jpvJjw8XLpvQgwCPU4MD1xvose9vIZL6xCSj6DHieGhx4mZQ48Tw0OPEzOHHieGhx4nZg49TgwPPU7MHHqcGB56nJg59DgxPPQ4MXPocWJ46HFi5tDjxPDQ48TMoceJ4aHHiZlDjxPDQ48TM4ceJ4aHHidmDj1ODA89TswcepwYHnqcmDn0ODE89Dgxc+hxYnjocWLm0OPE8NDjxMyhx4nhoceJmUOPE8NDjxMzhx4nhoceJ2YOPU4MDz1OzBx6nBgeepyYOfQ4MTz0ODFz6HFieOhxYubQ48Tw0OPEzKHHieGhx4mZQ48Tw0OPEzOHHieGhx4nZg49TgwPPU7MHHqcGB56nJg59DgxPPQ4MXPocWJ46HFi5tDjxPDQ48TMoceJ4aHHiZlDjxPDQ48TM4ceJ4aHHidmDj1ODA89TswcepwYHnqcmDn0ODE89Dgxc+hxYnjocWLm0OPE8NDjxMyhx4nhoceJmUOPE8NDjxMzhx4nhoceJ2YOPU4MDz1OzBx6nBgeepyYOfQ4MTz0ODFz6HFieOhxYubQ48Tw0OPEzKHHieGhx4mZQ48Tw0OPEzOHHieGhx4nZg49TgwPPU7MHHqcGB56nJg59DgxPPQ4MXPocWJ46HFi5tDjxPDQ48TMoceJ4aHHiZlDjxPDQ48TM4ceJ4aHHidmDj1ODA89TswcepwYHnqcmDn0ODE89Dgxc+hxYnjocWLm0OPE8NDjxMyhx4nhoceJmUOPkzxNSkqKhYXFR++iUSM7hbVfTfjW3VLvt0LXE8NBj5O8TnBw8KAhwxRSzolcTkp1cnbB7F66Y0IMAj1ODEBYWFhOq/xcQhIlTgwKPU6MQY6qPCYuwd7egRInBoUeJ4ZBqDwjM0thYY2BxJ2dXbKysqS7IcRo0OPESEDl3Xu466hySpzkA+hxYjCioqL1UjklTvIH9DgxHrqonBIn+QZ6nBgSjSqP2HvI3sGBEif5A3qcGBWo3NHJ5VbaE4Wj3xlI3N2914sXL6QNEWJw6HFiYOLj499X5ZQ4yX/Q48TYJCYmmq5ySpzkS+hxYnhMVDklTvIr9DjJDwiVX09NV7hbzqo1613d3Chxki+hx0k+ASpvae9wOSlVYXAEEh/m5SX1IyTfQY+T/ENaWpqTs4tC5ZQ4yffQ4yRfoVA5JU7MAXqc5DdklVPixEygx0mOEBsb6+/v7+09ztnF5cPH3sHBukbNWrXrKNo/TNzc3PC3h4SE8IdwyYeBHid6kpWVNXmyj6VlKUcn5ym+MxcvW7X/8PFcSdjmnYqWD5Ydew7hb/caOcbK2hpHlAMHDkijQ0jOQI8T3QgKCoK5AgKXqPiufH5N5Im4bj3c7ezsUlJSpGEiRG/ocaIDL1688PDw8B4/kQZ/bWLiElraO+zfz4k5yRHocaIVSNzZxWXdxi0KeTHZgyMcJuZhYWHSqBGiH/Q40Qpm4pS4KcnIzOrY2S02NlYaOEJ0gh4nmggODhk+coxCWMybglm5ja1tZmamNHyE6AE9TtQDH8FKvCb+XgkIXDJ5so80goToAT1O1AMfwUoKTzFvT0ZmlpW1NafkREfocaKeihUrveUnBpk3ZZLP1MDAhdIgEqIZepyoJDY21tHJWWEoxpREnoizd3CQxpEQzdDjRCX+/v5TfGcqDMWYGAsLC/4YOtELepyoxNt73OJlqxR6YkyMlbU1f32F6AU9TlTi4TFw7QZ+bFxlHJ2c+UFyohf0OFGJs4vL/sPHFXpiTAw9TnSEHicqoce1hB4nOkKPE5XQ41pCjxMdoceJSuhxLaHHiY7Q40Ql9LiW0ONER+hxohJ6XEvocaIj9DhRCT2uJfQ40RF6nKiEHtcSepzoCD1OVEKPawk9TnSEHicqoce1hB4nOkKPE5XQ41pCjxMdoceJSuhxLaHHiY7Q40Qlunt82YrgqtWqf/QPdes32GfCxv3mBor+vjP87j/8dfacBfcePFf0yZvJNx4X40/UIQ2iZuhxohJ9Pb5xcwRe1mXKlB0/wWfEKO9ixYoVLVr0/KXrim7ZA2UX+/RTqH/H7kNRJ8/06e+BLaTef6zoljeTPzyOAVf8XYzpocdJ7qOvxwcM9MTLOjrmrFic4TevfIWKoWHb5A7bdx2c4jszfOsusfjo+V9bIvZhFbsmzfYfOR4TdxEFFtGIReRSUiq6Pf7l36gPR/0g1jqXcA2LD578697Pv6AnNrj8+3WiJ5Jw9aZYcenyNSdPx6MFc/zg0HB0kx+YXqHHTQmevqHDRxX+BxvbuhF7Dis6yDl6LLZipcqlSpXGEX3Dph2KW/Nm6HGS++jr8bnzF+NlXa58hWkz/Y9En8p+E5zr0KotbhVYWdeIPffj3Yxn0vI/tG7jJFUffeTzzbQCBQqM9v4S60aeiEOLhYVF8q0MLML12NWTU9PbtP1//UuWtLx87aXKv5j4FRarV7fCv6PHfpGQmCJf5wF9+w/M/PVv8ZC0hx43JXgqXd264QWA+kTshdKly1y8ciN7BzkLFgfN9AtAMSdg0aSvvs1+U54NRk8aR83Q40Ql+nocioQo/xHmSyD0b76dmZH5O25y790PLYHfLUd94OgJ1HVsbFGn3n+MupNrF7EFFFgU11XQATpGMclnKhrB1p37b959CKH3cO8DF7i077hmXRg6LF66Crd+O302auHxSpWrYAK+7/Bxt649sLhr31HcNHL0ONQbN0eg1iX0uCnB0XTcl5MxKxeLOD8Tl9pweoRnsGmzFl9NmY5bd+49gqO7dY2azu064NCLifmIUd54OYm1Bg8dPn/RMhTXbt5HjWJV8Hqsi3gM8ryd/gQtXiPHjB0/ES2HomK69ejVpGlzRycXv7mBOO0TG8mJYPSkcdQMPU5Uovv7nAj2T0zBWjq0xksciHcvMYP+rFZteY8aMNAT0+1zCdeEx/v09xDt2a+PT/SZgvpSUmrd+g2we39avPiQYSOEsjfv2IsOSTfuweODPb2w/6Mxu8fFAQOHkPIVKpYtWw5OR7A6bhJzfF1Cj5uSuAtXGzayq1uvPp6abTsPiOP61eQ7NWp+tmnb7pOn43v3/Ryz71tpmeMn+OBJ/+FMArwPWV9PScNr5sr12/cePMecwLVLd6y4JGg1nkGczGGbByNPHj91vn6DRv7zFuImqB8ngli9UqXK6zZuRQGh40AeELhEPJKcCEZPGkfN0ONEJfp6HK7EjiQvYh/D3Bm7q/B4o8ZN5JuwK2IHgPHf4vHT5698/PHHX0+dgRYcGLp2d69Vuw72+ZIlLTH/wj5cuHBhyB27d/eevdEnu8exn6MWHi9WrFibtk5y5i34TtyX9tDjJgbTbZyE4Qlq3cYJasa51Esdj/1C3ApfixOvlWtCg0PDUcjXVcaMmxCyfvP2XQc9vUbiSJD26LdWrR2jTp7BTfB+4k93127Y0qyF/bIVwWiBx3G6hqJy5SqYm4v07jfAyspaHDxyIhg9aRw1Q48TlejrceylmGVjKiQW9xyMhsedXNqjxtkunIuTYnET9tsKFSuheIvHEUylsQhgfMyqRI0ZltxTfK4xJHQT6lc9jjRo2BgbERdnj0SfwpEmITFF3KQ99LgpwWmQPOYPn/0JI+M5mjt/MabeovFO+lM8Ryhe9fiO3Ydw/J4w+ZvN2/e079gZ03kcBn5++gcm6U2aNseU/HOPIc2at1R4HOdnaJHzfcgGepzkZ/T1+InYCyVKlsQrG3NwcV2laNGiO/cewU3hW3cVKlSoarXqmF9jN8NEW+yxCo+L6+PDRowWV7QHe3phURj/UlIqaiAucGM/R928pQPOwXHwQP1ajwetWotFTORxzl6mTFk8vISrN8VN2kOPmxKcA2Fa/eDJv1DD4x07u81ftAznavXqN8SJGhqh2h7ufVC86nEou1o1K/TEnH2G3zwclcVlsemz5nh/MQkFtoDZA7aAWvZ4ufIVkm7cQ4GsWReGzqLOiWD0pHHUDD1OVKL79XHsn/0HDBZXMHr16S8++ScSeSIOXkZ7l249cZYtGu89eI4WSFYsincv0YKzaSyiG+rps+eihgKwwXYdOonZOnbgEaO8catrl+64U4gAG0f7d8u+R2NM3EXUIvC+2CYeGHrK7dpDj5sSHIBt69ZD+vYfiEM4ngtMwNHuMcgTk2toHSdn4nWyeOmqZStDUOCoj0O+8C9m3NA3isjjp/FQjx6LRY1/cVTGBuvWb4ATPjEPkD0+YKBnuXLle/f9HJMJHAZM+TKa6tDjJPfJifc5zSf0uInBZDzuwtX14dsvX0uVP7iCXLicvOdAlJiVI3czniGixhH3hzMJKHCkFx9HefT8r+RbGfLHRnE4D9uyEzdhg0LfN+78LN+acu8Rjt/RMWfTH2eJlhwKPU5yH3pcSwzn8ZAjd/af/RmPXFr+hw/g8XwcepzkPvS4lhjO41PXJbr6xiLjV16WhU6Pawk9TnIfelxLhBMNFLdXWiD0SvU6KP4uxvTQ4yT3oce1xLjzcWToovhVB25du5fF+biW0OMk93nV43hdduvRK3uLiixbEYzt7Nh9SNGeowkN2yb/Atdr8+j5XxN9piT+dBf1vAXfafeXET0+amlCyJE7dx/9KTVpvq6CYa9Xv+HH/9C1u7v8pqUpuZvxrFHjJljRY5DnyNHjFLeqTuTx0+Jzq55eI1cFr89+k+6hx0nuk288vm7jVtyj/BWk12bYiNHoE//jT6iPRJ8a5PnyZzq0xHAe/+2P/0hVNjAmir/L9Ow5EFWuXHl4E/XDZ38OGjJM/qkcU3ImPrFNWydFo/aMGTdB/DDDBwhGTxpHzdDjRCVv8fips5emz5578nT82g1bUOw9dAyNW3fuR30w8qToHHkiDovYG8O27ESBRdGu8Pju/ZG4FZF/6G7F6tC58xdfuX57TsAitN9Of3IrLXPB4iBMk2/cefn+m8i+w8dxa0DgEvHBMrnlUlJq9kfy0+0HvfsNwD3i35VrQtGCLWNrYl10RsuhqJhWrR3RZ9yXk/E4o2POjho7Xmwz89e/Q9ZvRmdEfNZNJCR0E1quJt/ZsGkHCvxR4nuhcgzn8deCMcn+R71XWreRPukvgvGZPmsOnkrUEXsOOzq52Ldq833IBnErlI3OaHHr2uP0+Svo1rRZCxwGXN26YVKPVwL64JWAlx/6LAlaLRSPVxFqsQV0w5OS/jgLh+TJX/uiG6b/OAnDM4v6c48h2CaO0JUqV7GuUROvhFn+88WL8OzFJEz50QevkORbGctWhgQsXCq2gD9h9dqNYvsqgtGTxlEz9DhRyVs8vvz7daixP9Sxsf20eHELC4uGjexq1PysVKnSOBHevusg+mC3RJ+an9WqVbtOiZIlCxcuvCViH9qzexw1+ltZ1yhduoylZSkhyiZNmxctWhRbq1u/AXpi9XLlK+AUG7WNbV3xmd+hw0e9XNHKumzZcljx2A/n0Ch++xArIoUKFUKHbTsPJCSmoFHQrHlLSBz3hcfZoGFjtJQsaZn26LcpvjNFB7BwyQr5usr1lDTRDY+hTJmyBf77Y7kI9ny0t7BvJW5CLX/vVIQex/MirlMpguNrvfoNN2/fs2vf0ZYOrcXPUmJsvUaOOXosFsrGawnzdygew4sWTJ+HjxyLPg6t2uIAj5befT/H84sWPL9oEZvFdvCyRAFTz/Cbh26YQ+Dlhz6o8frByyP1/uNeffrjaH3hcvLgocNhc7geLz84HX3w5DZv6YC7wytn6fI1aMFBAiOQ/Qtr7xWsK42jZuhxopJ3etzJpf2j53+tCl6PurFdU+wSO/ceQS2+gSmuZmDfy8j8HUKE7stXqIh22eMwLPbert3dsRHsYBUrVYbB0QH/ogNWx1wYezJqzI/Qjp6oE67ejDp5BgVmT9jbMZkq9umnHTu7oYPwuKfXSGwQJwqosceiHUZAjX9xDMCpw5BhI3DAwMZhB7Rjbvjz0z+8v5iEOvbcZawrexz7PArs5Gi8m/EM+sCi+C0t8asAE32m4CashQMP/ha0y6HHixQpImbfiowcPU6eROPpaNehEwp4WXzN5076UxxoUSTduIdJNAooFc+UOKaKLwqh51s8jsOAfCEeU3gcp/Fi69DJVfx8uXxdRXgc516YsIvOCI4Bs+cswBFFLOLu8MIWP16vIhg9aRw1Q48TlbzT4+JtIsy+UUN2qDFzQS1+11B4XPwmBgIJYpqDeZDscfE/vXXv2Ru7IiJm39jx4HHMsrH7YS2cVhcsWFDoQPy0bMq9R5gyo8AJuFixarXqmPrB6cLjmEahc0zcRdS+M/xQK66Pn0u4hkfr0r4jXIB2cfYwYfI3qMX1ceFxPACcB0DQ8pcMj586j3ZxzBAel/9DDMzKsShqEXocBhRnYCI4cMKkeIXAziGhm0QjTqQwBUYhvIy8yeOnzl7CBkUf5FWP4zgtPO7o3E604KXi6OQCNXd27YoX1Zs8jlem6I+gGzzuNzdQbmnT1okeJwbmnR7funM/auFx8dsXr3pctufosV9gEQ5VeLytozPOZ0WwjwmPQ81iLXi8cOHCopY9Pn32XBSYYckrIpgXC4+L/4jgTR7HuTwODNi3sTXxk7Zv97iFhQVaRGAWtDdt1gI1Pf7O4HDbuo2T/O5F+NZdOCdLTk1fsDjIY5Cn+JY8niA8dyje6XE8HZWrVBVKxSmU6I/jwbARo1FgazhdU3jcf95CcSteGzi1eq3HcS94MeB0EC149suVKx+4eDk9TvIPunjcvXc/1Ih1jZplypTF3Fb2+Jn4RBQDBw/FrRmZv2M3EzvYOz2+7/BxFOKnTeH9LyZ+tT58O2pTPN7JtQs2eOX6bdTiMypv8jgK8buM2NtRI+Mn+GDxm29noqbHTcmIUd7VqlnhicPzWL5CxT0Ho9GIp8y2br32HTvjtQE1i9+YfNXjeI7E//gjPI4Cs2+8iho1blLHxlb0v3jlBjaL7eCQjPMzvLTwAsPBQxwk9h85jk1B3C3sW9Vv0Ei8dPEywxZWr90oPI6W6bPmYLNjx0+ExHH6GLBw6Qy/eWgXcWjVVn4H/n2D0ZPGUTP0OFGJLh4vXqIEzm0xh0Utfk1U9jh2NsypCxQogNPeZi3s0Sj2n3d6/OGzP9GnSJEikLK4gC5+/e5NHj8YeRI1dv5JX30r+mB11y7dcdeoxTFg6PBRqPF4IAvZ4ziXL1q0KJTRw70P9mdM5OHrn5/+gZvocRNz4XJy0Kq18KM8MUdw2MbLBidk4tcNEQy1KCDi0+evoLiVlokjPQo844k/3UXP3fsjk29l/HAmARNk8XvFCCbUeO2h59XkO1D/gyf/Qge8QsStCYkpmP5DxHixxZ67jBbMzTds2hF5/DT6i2k4EnXyDPR99mKSWEUWN9bC1rI/8vcKRk8aR83Q40Qlr3ocE1hxcgp/oT4UFYM69tyPqKE/1NiRUIurn8LjmGKPHvtFs+Yt5wQsEnsX7I8+Yl+9//BXqLZBw8ZwvfgvFhHYHJMj8bkUnJt7jRwj9nbMobAi9lvUdzOewd2YWOG0FwcVMf/CmTs6CI9nfyRQw9Rps6Ba3BcsjGk1ahwVMD1EnxWrX07KsOviEIVHgp18z4GoLt17ohHB1jBVx9ECd4RdXX4DDTXWFTs8rIRzAiyKm0TocX2D10PtOjaeXiPxBDm5tMcTquiQB0OPk9znVY+/V7JfzTDD0OO6B/PiyV/79uzVV8tnuj9k6HGS+2j0+Mo1ofS4NJSGBc8g0YI0jpqhx4lKNHr87MUkqFx8YdIMkz88TvII9DhRiUaPm3nocaIj9DhRCT2uJfQ40RF6nKiEHtcSepzoCD1OVEKPawk9TnSEHicqoce1hB4nOkKPE5XQ41pCjxMdoceJSuhxLaHHiY7Q40Ql9LiW0ONER+hxohJ6XEvocaIj9DhRCT2uJfQ40RF6nKiEHtcSepzoCD1OVOLm5rZjzyGFnhgTQ48THaHHiUq8vIav/Oe3uRkVsbGxTUlJkYaSEG3Q40Ql/v7+U3xf/h9mjIpYWpbKysqShpIQbdDjRCXh4eHiP89k3jfXU9MrVqwkjSMhmqHHiUownYSMMjJf/v9qzHtl8bJV3t7jpHEkRDP0OFFPr169wrfuVkiKeWf4JifRF3qcqCcxMbFRYzuFpJi3Z//h484uLtIIEqIH9DjRBKfk7xsc+XD8k4aPED2gx4kmsrKy7O0dziUkKWzFvDaTv5oaGBgojR0hOkGPE62kpaU1bmx3PTVd4SxGkVVr1g/z8pJGjRD9oMeJDiQmJkLlMXEJCnMxcrzHT6TESQ5BjxN9yMzMtLOzGz5yDCfmioRv3d2osV1QUJA0UoToDT1O9CQ4OMTa2rpPvwErV4feSnuiMJpZBWcnk3ym2NjY9urVi29skhyFHic68+LFi4iICC+v4ZaWpT4yY3B2snDhQv6ICvkA0OOEEGJs6HFCCDE29DghhBgbepwQQowNPU4IIcaGHieEEGNDjxNCiLGhxwkhxNjQ44QQYmzocUIIMTb0OCGEGBt6nBBCjA09TgghxoYeJ4QQY0OPE0KIkfnf//2/TIQIeKvjh3IAAAAASUVORK5CYII='/>"; // Tämä restin kanssa
                    //console.debug("item.imagesource: " + item.imagesource); //jostain syystä tulee "undefined" --> node js ei päästä läpi? vai filtteröikö dojo kentän pois? -- olikin toisessa paikassa kuin luulin

                    //Cannot call method 'replace' of undefined
                    //dojo.byId('content').innerHTML = dojo.byId('content').value.replace(/\n/g, "<br />");
                    
                    //TODO
                    //console.log(myTree.getNodesByItem(item)); //array objekteja - ensimmäinen näyttäisi olevan aina root node
                    //voisi kelata taulukot läpi etsien nimi-kenttiä jotka matchaavat (tekstiin tai) description kenttään 
                    //ja luoda linkki regexpillä tekstiin/descriptioniin
                    //console.log(item.items[2].table[0].name); //PM.O1
                    

                }
                //viitataan data-dojo-attach-pointiin desktop.html sivulla
//            }, this.treeNode);
            }, this.containerNode);
        },
       
        resize: function() {

            this.inherited(arguments);
            console.log("Resize event");
            //this.bcPane.resize();
            
            //jälleen ongelma korjaantui laittamalla viivettä :) - ei näin!!!! :)
//            var delay=300;//0,3 seconds
//                    setTimeout(function(){
//                        registry.byId("borderContainer").resize();
//                    },delay);
            registry.byId("borderContainer").resize();
        }
        
    });
     
});

//function replaceNewLineAndTab(string){
//
//    string.replace(new RegExp('\r?\n?\t','g'), '<br />');
//    
//    return newstring;
//}