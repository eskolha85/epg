define([
    "dojo",
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/store/JsonRest",
    "dijit/Tree",
    "dijit/registry",
    "dijit/layout/BorderContainer",
    "dijit/layout/ContentPane",
    "dojo/text!./templates/desktop.html"
], function(
dojo,
declare,
_WidgetBase,
_TemplatedMixin,
_WidgetsInTemplateMixin,
JsonRest,
Tree,
registry,
BorderContainer,
ContentPane,
template){

    return declare("Desktop", [BorderContainer, ContentPane, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: template,
        widgetsInTemplate: true,
        
        constructor: function() {
              console.log(this.containerNode);  //null
              //console.log(this.bcPane); //undefined
        },
        
        postCreate: function(){
            // do something after desktop template ready. Note: My children might not be 
            // ready yet, so dont' access them here!
            this.inherited(arguments);
            console.log("postCreate");
            //console.log(this.bcPane);
            //console.log(this.containerNode);
            

        },
        startup: function() {
            // This fires after my children are available
            this.inherited(arguments);
            console.log("startup");
            
            var reststore = new JsonRest({
                //target:"data/",         // << adapt URL
                target: "http://localhost:3000/guides/",
                mayHaveChildren: function(object){
                    // if true, we might be missing the data, false and nothing should be done
                    return "children" in object;
                },
                getChildren: function(object, onComplete, onError){

                    // this.get calls 'mayHaveChildren' and if this returns true, it will load whats needed, overwriting the 'true' into '{ item }'
                    this.get(object._id).then(function(fullObject){
                        // copy to the original object so it has the children array as well.
                        object.children = fullObject.children;
                        // now that full object, we should have an array of children
                        onComplete(fullObject.children);
                    }, function(error){
                        // an error occurred, log it, and indicate no children
                        console.error(error);
                        onComplete([]);
                    });
                },
                getRoot: function(onItem, onError){
                    // get the root object, we will do a get() and callback the result
                    //this.get("VSETEST").then(onItem, onError);
                    this.get("vse-tree").then(onItem, onError);
                },
                getLabel: function(object){
                    // just get the name (note some models makes use of 'labelAttr' as opposed to simply returning the key 'name')
                    return object.name;
                }
            });
            
            var myTree = new Tree({

                model: reststore, //reststore suoraan tänne, ei modelin kautta
                showRoot: true,
                
                //onLoad() Called when tree finishes loading and expanding.
                onLoad: function() {
                    
                  console.log("Tree onLoad()");
                  //haetaan widgettiä dijit-rekisteristä sen nimellä
                  registry.byId("borderContainer").resize();
                  //this.bcPane.resize(); //taustalta vaikuttaisi katoavan koko bordercontainer
                  //this.resize(); -- ei toimi
                 
                },

                //onClick(item, node, evt) Callback when a tree node is clicked
                onClick: function(item) {
                    
                    for (var i = 0; i < item.items.length; i++) {
                        var group = item.items[i];

                        var allPropertyNames = Object.keys(group);
                        for (var j=0; j<allPropertyNames.length; j++) {
                            var name = allPropertyNames[j];
                            var value = group[name];
                            // Do something
                            console.log(name);
                            //console.log(value);
                        }
                    }
                    
                    var str = item.description; //täytyi ottaa talteen muuttujaan ensin - muuten ei toimi jos suoraan item.description.replace()
                    //var res = str.replace(/(\n)+/, '<br/>'); // muuttaa vain 1. \n --> <br/> - muut filtteröidään pois

                    //katsotaan onko "undefined" toimii myös jos muuttujaa ei esitelty
                    if(!(typeof str==="undefined")){
                        var res = str.replace(new RegExp('\r?\n','g'), '<br />');
                    }
                    
                    var content = "<h1>" + item.name + "</h1><p>" + res + "</p>";
                    
                    for(var i=0; i< item.items.length; i++){
                        //console.log(item.items[i].name);
                        
                        //löytyykö 'name' -key
                        if (!(typeof (item.items[i].name)==="undefined")){
                            content += "<h3>" + item.items[i].name + "</h3>";
                        }
                        
                        //löytyykö 'status' -key
                        if (!(typeof (item.items[i].status)==="undefined")){
                            
                            var s = item.items[i].status;
                            var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                            
                            content += "<p>" + r + "</p>";
                        }
                        
                        //löytyykö 'description' -key
                        if (!(typeof (item.items[i].description)==="undefined")){
                            
                            var s = item.items[i].description;
                            var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                            
                            content += "<p>" + r + "</p>";
                        }
                        
                        //löytyykö 'image' -key
                        if (!(typeof (item.items[i].image)==="undefined")){
                            content += "<img src='" + item.items[i].image + "'/>";
                        }
                        
                        //löytyykö 'table' -key
                        if (!(typeof (item.items[i].table)==="undefined")){
                            
                            content += "<table style='width:100%'>";
                            
                            for(var j=0; j< item.items[i].table.length; j++){
                                
                                if (j % 2 === 0 ){
                                    content += "<tr class='alt-row'>";
                                } else {
                                    content += "<tr>";
                                }

                                //löytyykö tablesta 'name' -key
                                if (!(typeof (item.items[i].table[j].name)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].name + "</th>";
                                    } else {
                                        content += "<td>" + item.items[i].table[j].name + "</td>";
                                    }
                                    
                                }
                                
                                //löytyykö tablesta 'description' -key
                                if (!(typeof (item.items[i].table[j].description)==="undefined")){
                                    //console.log("tablesta löytyi description");
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].description + "</th>";
                                    } else {
                                        var s = item.items[i].table[j].description;
                                        var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                                        content += "<td>" + r + "</td>";
                                    }
                                }
                                
                                //löytyykö tablesta 'source' -key
                                if (!(typeof (item.items[i].table[j].source)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].source + "</th>";
                                    } else {
                                        content += "<td>" + item.items[i].table[j].source + "</td>";
                                    }
                                }
                                
                                //löytyykö tablesta 'destination' -key
                                if (!(typeof (item.items[i].table[j].destination)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].destination + "</th>";
                                    } else {
                                        content += "<td>" + item.items[i].table[j].destination + "</td>";
                                    }
                                }
                                
                                //löytyykö tablesta 'role' -key
                                if (!(typeof (item.items[i].table[j].role)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].role + "</th>";
                                    } else {
                                        var s = item.items[i].table[j].role;
                                        var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                                        content += "<td>" + r + "</td>";
                                    }
                                }
                                
                                //löytyykö tablesta 'input_products' -key
                                if (!(typeof (item.items[i].table[j].input_products)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].input_products + "</th>";
                                    } else {
                                        var s = item.items[i].table[j].input_products;
                                        var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                                        content += "<td>" + r + "</td>";
                                    }
                                }
                                
                                //löytyykö tablesta 'output_products' -key
                                if (!(typeof (item.items[i].table[j].output_products)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].output_products + "</th>";
                                    } else {
                                        var s = item.items[i].table[j].output_products;
                                        var r = s.replace(new RegExp('\r?\n','g'), '<br />');
                                        content += "<td>" + r + "</td>";
                                    }
                                }
                                
                                //löytyykö tablesta 'abbreviation' -key
                                if (!(typeof (item.items[i].table[j].abbreviation)==="undefined")){
                                    
                                    if(j===0) {
                                        content += "<th>" + item.items[i].table[j].abbreviation + "</th>";
                                    } else {
                                        content += "<td>" + item.items[i].table[j].abbreviation + "</td>";
                                    }
                                }
                                content += "</tr>";
                            }
                            content += "</table>";
                        }
                    }
                    dojo.byId('contentId').innerHTML = content;
                }
                //viitataan data-dojo-attach-pointiin desktop.html sivulla
            }, this.containerNode);
        },
       
        resize: function() {

            this.inherited(arguments);
            console.log("Resize event");
            registry.byId("borderContainer").resize();
        }
    });
});